OpenHarmony 轻量系统
=====================

本仓是  OpenHarmony 轻量系统 代码仓

基于OpenHarmony全量代码，删除了所有不必要的代码目录：

```sh
$ du -d 1 -h .
50M     ./docs
205M    ./vendor
152K    ./applications
6.0M    ./test
1.2M    ./kernel
276K    ./utils
1.7M    ./foundation
72K     ./domains
3.9M    ./third_party
2.8M    ./base
684K    ./build
272M    .
```

目前master分支来自OpenHarmony-1.0，仅支持海思Hi3861芯片。



## 快速上手

1. 下载编译工具链；

   ```sh
   # 下载、解压 gn/ninja/riscv-gcc
   URL_PREFIX=https://repo.huaweicloud.com/harmonyos/compiler
   
   DOWNLOAD_DIR=~/Downloads     # 下载目录，可自行修改
   TOOLCHAIN_DIR=~/hostools     # 工具链存放目录，可自行修改
   
   # 若目录不存在，则先创建
   [ -e $DOWNLOAD_DIR ] || mkdir $DOWNLOAD_DIR
   [ -e $TOOLCHAIN_DIR ] || mkdir -p $TOOLCHAIN_DIR
   
   # 下载 压缩包：
   wget -P $DOWNLOAD_DIR $URL_PREFIX/gn/1523/linux/gn.1523.tar
   wget -P $DOWNLOAD_DIR $URL_PREFIX/ninja/1.9.0/linux/ninja.1.9.0.tar
   wget -P $DOWNLOAD_DIR $URL_PREFIX/gcc_riscv32/7.3.0/linux/gcc_riscv32-linux-7.3.0.tar.gz
   
   # 解压 压缩包：
   tar -C $TOOLCHAIN_DIR/ -xvf $DOWNLOAD_DIR/gn.1523.tar
   tar -C $TOOLCHAIN_DIR/ -xvf $DOWNLOAD_DIR/ninja.1.9.0.tar
   tar -C $TOOLCHAIN_DIR/ -xvf $DOWNLOAD_DIR/gcc_riscv32-linux-7.3.0.tar.gz
   ```

2. 配置环境变量；

   ```sh
   # 向 ~/.bashrc 中追加gn/ninja/LLVM/hc-gen路径配置：
   cat | tee -a ~/.bashrc <<EOF
   TOOLCHAIN_DIR=$TOOLCHAIN_DIR
   export PATH=\$TOOLCHAIN_DIR/gn:\$PATH
   export PATH=\$TOOLCHAIN_DIR/ninja:\$PATH
   export PATH=\$TOOLCHAIN_DIR/gcc_riscv32/bin:\$PATH
   export PATH=~/.local/bin:\$PATH       # 用户pip二进制工具目录
   EOF
   ```

3. 下载python包；

   ```sh
   pip3 install scons kconfiglib pycryptodome six ecdsa
   ```

4. 下载代码；

   ```sh
   git clone https://gitee.com/swxu/openharmony_mini.git
   ```

5. 编译代码；

   ```sh
   cd openharmony_wifiiot
   python build.py wifiiot
   ```



## 参考链接

1. HarmonyOS物联网应用开发: https://gitee.com/hihopeorg/HarmonyOS-IoT-Application-Development
2. HiBurn使用指南：https://harmonyos.51cto.com/posts/1197
3. OpenHarmony官网：https://www.openharmony.cn/

